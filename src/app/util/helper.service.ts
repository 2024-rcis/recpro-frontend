import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HelperService {

  constructor() { }

  static createApiList(unformatted: string[]): string {
    return unformatted.join(',');
  }
}
