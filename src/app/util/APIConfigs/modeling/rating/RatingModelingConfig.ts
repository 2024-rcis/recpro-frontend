
export class RatingModelingConfig {

  api: RatingApi;

  constructor(baseUrl: string) {
    this.api = new RatingApi(baseUrl);
  }

  private getBaseUrl(): string {
    return this.api.baseUrl;
  }

  getAll(): string {
    return this.getBaseUrl() + this.api.rating.getAll;
  }

  create(): string {
    return this.getBaseUrl() + this.api.rating.create;
  }

  getByElementId(id: string): string {
    return this.getBaseUrl() + this.api.rating.getByElementId + id;
  }

  addToBpmElement(bpmElementId: string, ratingId: string): string {
    return this.getBaseUrl() + this.api.rating.addToBpmElement + this.api.rating.bpmElementId + bpmElementId + this.api.rating.ratingId + ratingId;
  }

  setDefaultRating(ratingId: string): string {
    return this.getBaseUrl() + this.api.rating.setDefaultRating + ratingId;
  }

  getDefaultRating(): string {
    return this.getBaseUrl() + this.api.rating.getDefaultRating;
  }

  delete(ratingId: string): string {
    return this.getBaseUrl() + this.api.rating.delete + ratingId;
  }
}

class RatingApi {
  baseUrl: string;
  rating: Rating = new Rating();

  constructor(baseUrl: string) {
    this.baseUrl = baseUrl + this.rating.baseUrl;
  }
}

class Rating {
  baseUrl: string = '/rating';
  getAll: string = '/getAll';
  create: string = '/create';
  getByElementId: string = '/getByElementId/'
  addToBpmElement: string = '/addToBpmElement?';
  bpmElementId: string = '&bpmElementId=';
  ratingId: string = '&ratingId=';
  setDefaultRating: string = '/setDefaultRating/';
  delete: string = '/delete/';
  getDefaultRating: string = '/getDefaultRating';
}
