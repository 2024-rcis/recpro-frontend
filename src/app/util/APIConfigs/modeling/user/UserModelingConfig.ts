export class UserModelingConfig {
  api: UserApi;

  constructor(baseUrl: string) {
    this.api = new UserApi(baseUrl);
  }

  private getBaseUrl(): string {
    return this.api.baseUrl;
  }

  create(): string {
    return this.getBaseUrl() + this.api.user.create;
  }

  getAll(): string {
    return this.getBaseUrl() + this.api.user.getAll;
  }

  getById(id: string): string {
    return this.getBaseUrl() + this.api.user.getById + id;
  }

  delete(id: string): string {
    return this.getBaseUrl() + this.api.user.delete + id;
  }
}

class UserApi {
  baseUrl: string;
  user: User = new User();

  constructor(baseUrl: string) {
    this.baseUrl = baseUrl + this.user.baseUrl;
  }
}

class User {
  baseUrl: string = '/user';
  create: string = '/create';
  getAll: string = '/getAll';
  getById: string = '/getById/';
  delete: string = '/delete/';
}
