import {HelperService} from "../../../helper.service";

export class AttributeModelingConfig {

  api: AttributeModelingApi;

  constructor(baseUrl: string) {
    this.api = new AttributeModelingApi(baseUrl);
  }

  private getBaseUrl(): string {
    return this.api.baseUrl;
  }

  create(): string {
    return this.getBaseUrl() + this.api.attribute.create;
  }
  createWithElement(elementId: string): string {
    return this.getBaseUrl() + this.api.attribute.createWithElement + elementId;
  }
  createWithElements(ids: string[]): string {
    return this.getBaseUrl() + this.api.attribute.createWithElements + this.api.attribute.elementIds + HelperService.createApiList(ids);
  }

  getAll(): string {
    return this.getBaseUrl() + this.api.attribute.getAll;
  }

  getById(id: string): string {
    return this.getBaseUrl() + this.api.attribute.getById + id;
  }

  getByIds(ids: string[]): string {
    return this.getBaseUrl() + this.api.attribute.getByIds + HelperService.createApiList(ids);
  }

  getByActivityId(id: string): string {
    return this.getBaseUrl() + this.api.attribute.getByActivityId + id;
  }

  getByActivityIds(ids: string[]): string {
    return this.getBaseUrl() + this.api.attribute.getByActivityIds + HelperService.createApiList(ids);
  }

  delete(id: string) {
    return this.getBaseUrl() + this.api.attribute.delete + id;
  }
}

class AttributeModelingApi {
  baseUrl: string;
  attribute: Attribute = new Attribute();

  constructor(baseUrl: string) {
    this.baseUrl = baseUrl + this.attribute.baseUrl;
  }
}

class Attribute {
  baseUrl: string = '/attribute';
  createWithElement: string = '/createWithElement/';
  create: string = '/create';
  getAll: string = '/getAll';
  getById: string = '/getById/';
  getByIds: string = '/getByIds?';
  attributeIds: string = '&attributeIds=';
  getByActivityId: string = '/getByActivityId/';
  getByActivityIds: string = '/getByActivityIds?';
  activityIds: string = '&activityIds=';
  createWithElements: string = '/createWithElements?';
  elementIds: string = '&elementIds=';
  delete: string = '/delete/';
}
