import {BpmElementModelingConfig} from "./BpmElementModelingConfig";
import {BpmProcessModelingConfig} from "./BpmProcessModelingConfig";

export class BpmModelingConfig {
  api: BpmModelingApi;
  process: BpmProcessModelingConfig;
  bpmElement: BpmElementModelingConfig;

  constructor(baseUrl: string) {
    this.api = new BpmModelingApi(baseUrl);
    this.bpmElement = new BpmElementModelingConfig(this.api.baseUrl);
    this.process = new BpmProcessModelingConfig(this.api.baseUrl);
  }
}

class BpmModelingApi {
  baseUrl: string;
  modeling: Modeling = new Modeling();

  constructor(baseUrl: string) {
    this.baseUrl = baseUrl + this.modeling.baseUrl;
  }
}

class Modeling {
  baseUrl: string = '/bpm';
}
