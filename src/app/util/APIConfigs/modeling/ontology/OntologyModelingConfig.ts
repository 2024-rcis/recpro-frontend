export class OntologyModelingConfig {

  api: OntologyApi;

  constructor(baseUrl: string) {
    this.api = new OntologyApi(baseUrl);
  }

  private getBaseUrl(): string {
    return this.api.baseUrl;
  }

  clear(): string {
    return this.getBaseUrl() + this.api.ontology.clear;
  }

  clearAttributes(): string {
    return this.getBaseUrl() + this.api.ontology.clearAttributes;
  }

  clearRatings(): string {
    return this.getBaseUrl() + this.api.ontology.clearRatings;
  }

  clearBpm(): string {
    return this.getBaseUrl() + this.api.ontology.clearBpm;
  }
}

class OntologyApi {
  baseUrl: string;
  ontology: Ontology = new Ontology();

  constructor(baseUrl: string) {
    this.baseUrl = baseUrl + this.ontology.baseUrl;
  }
}

class Ontology {
  baseUrl: string = '/ontology';
  clear: string = '/clear';
  clearAttributes: string = '/clearAttributes';
  clearRatings: string = '/clearRatings';
  clearBpm: string = '/clearBpm';
}
