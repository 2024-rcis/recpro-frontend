import {FilterType} from "../../../../../model/RecPro/filter/FilterType";

export class FilterExecutionConfig {
  api: FilterExecutionApi;

  constructor(baseUrl: string) {
    this.api = new FilterExecutionApi(baseUrl);
  }

  private getBaseUrl(): string {
    return this.api.baseUrl;
  }

  createByFilter(): string {
    return this.getBaseUrl() + this.api.filterExecution.createByFilter;
  }

  createByFilterId(filterId: string, tasklistId: number, filterType: FilterType): string {
    return this.getBaseUrl() + this.api.filterExecution.createByFilterId + filterId + this.api.filterExecution.tasklistId + tasklistId + this.api.filterExecution.filterType + filterType;
  }

  save(): string {
    return this.getBaseUrl() + this.api.filterExecution.save;
  }
}

class FilterExecutionApi {
  baseUrl: string;
  filterExecution: FilterExecution = new FilterExecution();

  constructor(baseUrl: string) {
    this.baseUrl = baseUrl + this.filterExecution.baseUrl;
  }
}

class FilterExecution {
  baseUrl: string = '/execution';
  createByFilter: string = '/createByFilter';
  createByFilterId: string = '/createByFilterId?filterId=';
  tasklistId: string = '&tasklistId=';
  save: string = '/save';
  filterType: string = '&filterType=';
}
