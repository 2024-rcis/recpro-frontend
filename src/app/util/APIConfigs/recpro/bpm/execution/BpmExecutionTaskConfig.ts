export class BpmExecutionTaskConfig {
  taskApi: TaskApi;

  constructor(baseUrl: string) {
    this.taskApi = new TaskApi(baseUrl);
  }

  private getTaskBaseUrl(): string {
    return this.taskApi.baseUrl;
  }

  claimTask(): string {
    return this.getTaskBaseUrl() + this.taskApi.task.claim;
  }

  startTask(): string {
    return this.getTaskBaseUrl() + this.taskApi.task.start;
  }

  cancelTask(): string {
    return this.getTaskBaseUrl() + this.taskApi.task.cancel;
  }

  unclaimTask(): string {
    return this.getTaskBaseUrl() + this.taskApi.task.unclaim;
  }

  completeTask(): string {
    return this.getTaskBaseUrl() + this.taskApi.task.complete;
  }

  stopTask(): string {
    return this.getTaskBaseUrl() + this.taskApi.task.stop;
  }

  getLatestByUserId() {
    return this.getTaskBaseUrl() + this.taskApi.task.getLatestByUserId;
  }
}

class TaskApi {
  baseUrl: string;
  task: Task = new Task();

  constructor(baseUrl: string) {
    this.baseUrl = baseUrl + this.task.baseUrl;
  }
}

class Task {
  baseUrl: string = '/task-dto';
  claim: string = '/claim';
  complete: string = '/complete';
  unclaim: string = '/unclaim';
  start: string = '/start';
  cancel: string = '/cancel';
  stop: string = '/stop';
  getLatestByUserId: string = '/getLatestByUserId';
}




