import {TasklistOrder} from "../../../../../model/BPM/execution/TasklistOrder";

export class BpmExecutionTasklistConfig {
  tasklistApi: TasklistApi;

  constructor(baseUrl: string) {
    this.tasklistApi = new TasklistApi(baseUrl);
  }

  getTasklistByAssignee(orderBy: TasklistOrder, ascending: boolean): string {
    return this.tasklistApi.baseUrl + this.tasklistApi.tasklist.getByAssigneeId + '?order=' + orderBy + '&ascending=' + ascending;
  }

  getAllTasks(): string {
    return this.tasklistApi.baseUrl + this.tasklistApi.tasklist.getAll;
  }

  filterTasklist(): string {
    return this.tasklistApi.baseUrl + this.tasklistApi.tasklist.filter;
  }
}

class TasklistApi {
  baseUrl: string;
  tasklist: Tasklist = new Tasklist();

  constructor(baseUrl: string) {
    this.baseUrl = baseUrl + this.tasklist.baseUrl;
  }
}

class Tasklist {
  baseUrl: string = '/tasklist';
  getByAssigneeId: string = '/getByAssigneeId';
  getAll: string = '/getAllTasks';
  filter: string = '/filter';
}
