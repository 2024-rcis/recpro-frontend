import {BpmExecutionConfig} from "./execution/BpmExecutionConfig";

export class BpmConfig {

  api: BpmApi;

  bpmExecutionApi: BpmExecutionConfig;
  constructor(baseUrl: string) {
    this.api = new BpmApi(baseUrl)
    this.bpmExecutionApi = new BpmExecutionConfig(this.api.baseUrl);
  }
}

class BpmApi {
  baseUrl: string;
  bpm: Bpm = new Bpm();

  constructor(baseUrl: string) {
    this.baseUrl = baseUrl + this.bpm.baseUrl;
  }
}

class Bpm {
  baseUrl = '/bpm';
}
