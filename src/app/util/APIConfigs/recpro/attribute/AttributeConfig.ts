import {AttributeExecutionConfig} from "./AttributeExecutionConfig";

export class AttributeConfig {
  api: AttributeApi;
  attributeExecution: AttributeExecutionConfig;


  constructor(baseUrl: string) {
    this.api = new AttributeApi(baseUrl);
    this.attributeExecution = new AttributeExecutionConfig(this.api.baseUrl);
  }
}

class AttributeApi {
  baseUrl: string;
  attribute: Attribute = new Attribute();

  constructor(baseUrl: string) {
    this.baseUrl = baseUrl + this.attribute.baseUrl;
  }
}

class Attribute {
  baseUrl: string = '/attribute';
}
