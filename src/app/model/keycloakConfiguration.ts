export class KeycloakConfiguration {
  issuer: string = 'https://auth.recpro.com.de/';
  realm: string = 'localhost';
  clientId: string = 'frontend';

  constructor(issuer:string, realm: string, clientId: string) {
    this.issuer = issuer;
    this.realm = realm;
    this.clientId = clientId;
  }
}
