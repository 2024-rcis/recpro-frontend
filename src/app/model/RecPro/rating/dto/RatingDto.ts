import {Rating} from "../modelig/Rating";
import {RatingInstance} from "../execution/RatingInstance";

export class RatingDto {
  rating: Rating;
  instance: RatingInstance;

  constructor() {
    this.rating = new Rating();
    this.instance = new RatingInstance();
  }
}
