import {RatingType} from "./RatingType";

export class Rating {
  id: string;
  name: string;
  description: string;
  ratingType: RatingType;

  constructor() {
    this.id = '';
    this.name = '';
    this.description = '';
    this.ratingType = RatingType.BINARY;
  }
}
