export class ScaleDetail {
  id: string;
  label: string;
  value: number;
  position: number;

  constructor() {
    this.id = '';
    this.label = '';
    this.value = -1;
    this.position = -1;
  }
}
