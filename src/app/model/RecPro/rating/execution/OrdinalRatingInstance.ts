import {RatingInstance} from "./RatingInstance";

export class OrdinalRatingInstance extends RatingInstance {
  value: number;

  constructor() {
    super();
    this.value = -1;
  }
}
