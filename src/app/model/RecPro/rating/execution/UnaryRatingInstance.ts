import {RatingInstance} from "./RatingInstance";

export class UnaryRatingInstance extends RatingInstance {
  value: boolean;

  constructor() {
    super();
    this.value = false;
  }
}
