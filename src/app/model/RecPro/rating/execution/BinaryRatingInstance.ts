import {RatingInstance} from "./RatingInstance";

export class BinaryRatingInstance extends RatingInstance {
  value: boolean;

  constructor() {
    super();
    this.value = false;
  }
}
