import {RatingType} from "../modelig/RatingType";

export class RatingInstance {
  id: number;
  recproElementId: string;
  recproElementInstanceId: string;
  ratingId: string;
  ratingType: RatingType;
  recproProcessInstanceId: string;
  userId: string;

  constructor() {
    this.id = -1;
    this.recproElementId = '';
    this.recproElementInstanceId = '';
    this.ratingId = '';
    this.ratingType = RatingType.BINARY;
    this.recproProcessInstanceId = '';
    this.userId = '';
  }
}
