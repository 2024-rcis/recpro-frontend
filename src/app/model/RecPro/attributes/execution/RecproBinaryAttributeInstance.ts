import {RecproAttributeInstance} from "./RecproAttributeInstance";

export class RecproBinaryAttributeInstance extends RecproAttributeInstance {
  value: boolean;

  constructor() {
    super();
    this.value = false;
  }
}
