import {RecproAttributeInstance} from "./RecproAttributeInstance";

export class RecproTextualAttributeInstance extends RecproAttributeInstance {
  value: string;

  constructor() {
    super();
    this.value = '';
  }
}
