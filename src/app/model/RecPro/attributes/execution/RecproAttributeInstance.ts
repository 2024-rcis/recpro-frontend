import {RecproAttributeType} from "../modeling/RecproAttributeType";

export class RecproAttributeInstance {
  id: number;
  recproElementId: string;
  recproElementInstanceId: string;
  recproAttributeId: string;
  attributeType: RecproAttributeType;

  constructor() {
    this.id = -1;
    this.recproElementId = '';
    this.recproElementInstanceId = '';
    this.recproAttributeId = '';
    this.attributeType = RecproAttributeType.TEXT;
  }
}
