import {RecproAttributeType} from "./RecproAttributeType";

export class RecproAttribute {
  id: string = '';
  name: string = '';
  description: string = '';
  bpmsAttributeId: string = '';
  attributeType: RecproAttributeType = RecproAttributeType.TEXT;
  fromUrl: boolean = false;
  url: string = '';

  constructor() {
  }
}
