export enum RecproAttributeType {
  TEXT= 'TEXT',
  NUMERIC = 'NUMERIC',
  OBJECT = 'OBJECT',
  BINARY = 'BINARY',
  META = 'META'
}
