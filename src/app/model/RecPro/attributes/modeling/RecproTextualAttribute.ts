import {RecproAttribute} from "./RecproAttribute";
import {RecproAttributeType} from "./RecproAttributeType";

export class RecproTextualAttribute extends RecproAttribute {
  defaultValue: string = '';
  override attributeType: RecproAttributeType = RecproAttributeType.TEXT;

  constructor() {
    super();
  }
}
