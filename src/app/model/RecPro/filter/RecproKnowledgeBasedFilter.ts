import {RecproFilter} from "./RecproFilter";
import {RecproElement} from "../../BPM/modeling/RecproElement";
import {RecproAttribute} from "../attributes/modeling/RecproAttribute";
import {FilterType} from "./FilterType";

export class RecproKnowledgeBasedFilter extends RecproFilter {
  bpmElements: RecproElement[] = [];
  attributes: RecproAttribute[] = [];
  allInputElements: boolean = false;
  allInputAttributes: boolean = false;

  constructor() {
    super();
    this.filterType =FilterType.KNOWLEDGE_BASED;
    this.bpmElements = [];
    this.attributes = [];
    this.allInputAttributes = false;
    this.allInputElements = false;
  }
}
