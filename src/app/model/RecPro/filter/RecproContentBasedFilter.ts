import {RecproFilter} from "./RecproFilter";
import {FilterType} from "./FilterType";
import {RecproElement} from "../../BPM/modeling/RecproElement";
import {RecproAttribute} from "../attributes/modeling/RecproAttribute";
import {Rating} from "../rating/modelig/Rating";

export class RecproContentBasedFilter extends RecproFilter {

  bpmElements: RecproElement[] = [];
  attributes: RecproAttribute[] = [];
  ratings: Rating[] = [];
  allInputElements: boolean = false;
  allInputAttributes: boolean = false;
  allInputRatings: boolean = false;

  constructor() {
    super();
    this.filterType = FilterType.CONTENT_BASED;
    this.bpmElements = [];
    this.attributes = [];
    this.ratings = [];
    this.allInputAttributes = false;
    this.allInputElements = false;
    this.allInputRatings = false;
  }

}
