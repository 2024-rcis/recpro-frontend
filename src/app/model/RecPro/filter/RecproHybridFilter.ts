import {RecproFilter} from "./RecproFilter";
import {FilterType} from "./FilterType";

export class RecproHybridFilter extends RecproFilter {

  filters: RecproFilter[] = [];

  constructor() {
    super();
    this.filterType = FilterType.HYBRID;
    this.filters = [];
  }

}
