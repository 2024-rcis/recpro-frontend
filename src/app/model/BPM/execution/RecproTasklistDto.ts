import {RecproTaskDto} from "./RecproTaskDto";
import {
  RecproFilterInstanceDto
} from "../../../components/bpm/bpm-execution/filter-dialog/model/RecproFilterInstanceDto";

export class RecproTasklistDto {
  id: number;
  assigneeId: string;
  tasks: RecproTaskDto[];
  date: Date;
  filter: RecproFilterInstanceDto;
  knowledgeBasedFilter: RecproFilterInstanceDto;
  baseFilter: RecproFilterInstanceDto;

  constructor() {
    this.id = -1;
    this.tasks = [];
    this.date = new Date();
    this.filter = new RecproFilterInstanceDto();
    this.assigneeId = '';
    this.knowledgeBasedFilter = new RecproFilterInstanceDto();
    this.baseFilter = new RecproFilterInstanceDto();
  }
}
