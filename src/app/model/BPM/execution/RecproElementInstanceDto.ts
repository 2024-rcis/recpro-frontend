import {RecproAttributeInstanceDto} from "../../RecPro/attributes/RecproAttributeInstanceDto";

export class RecproElementInstanceDto {
  id: number;
  assigneeId: string;
  likeliness?: number;
  attributes: RecproAttributeInstanceDto[];
  recproElementInstanceId: string;
  recproElementId: string;

  constructor() {
    this.id = -1;
    this.assigneeId = '';
    this.likeliness = undefined;
    this.attributes = [];
    this.recproElementId = '';
    this.recproElementInstanceId = '';
  }
}
