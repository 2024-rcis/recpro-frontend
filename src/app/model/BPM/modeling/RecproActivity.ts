import {RecproElement} from "./RecproElement";
import {RecproRole} from "./RecproRole";

export class RecproActivity extends RecproElement{
  roles: RecproRole[];

  constructor() {
    super();
    this.roles = [];
  }
}
