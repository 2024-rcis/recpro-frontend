import {RecproElement} from "./RecproElement";
import {RecproActivity} from "./RecproActivity";
import {RecproRole} from "./RecproRole";

export class RecproProcessModel extends RecproElement {

  xml: string;
  activities: RecproActivity[];
  roles: RecproRole[];

  constructor() {
    super();
    this.activities = [];
    this.roles = [];
    this.xml = '';
  }

}
