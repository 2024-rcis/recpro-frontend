import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {DashboardComponent} from "./components/dashboard/dashboard.component";
import {UserComponent} from "./components/user/user.component";
import {BpmComponent} from "./components/bpm/bpm.component";
import {BpmModelingComponent} from "./components/bpm/bpm-modeling/bpm-modeling.component";
import {BpmExecutionComponent} from "./components/bpm/bpm-execution/bpm-execution.component";
import {ErrorComponent} from "./layout/error/error.component";
import {ActivityComponent} from "./components/bpm/bpm-modeling/recpro-element/activity/activity.component";
import {ProcessComponent} from "./components/bpm/bpm-modeling/recpro-element/process/process.component";
import {
  RatingModelingBaseComponent
} from "./components/rating/rating-modeling/rating-modeling-base/rating-modeling-base.component";
import {OntologyBaseComponent} from "./components/ontology/ontology-base/ontology-base.component";
import {FilterModelingComponent} from "./components/filter/filter-modeling/filter-modeling.component";
import {AttributeComponent} from "./components/attribute/attribute.component";
import {AttributeModelingComponent} from "./components/attribute/attribute-modeling/attribute-modeling.component";
import {AuthGuard} from "./auth/auth.guard";
import {AccessDeniedComponent} from "./layout/access-denied/access-denied.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard]},
  { path: 'user', component: UserComponent, canActivate: [AuthGuard], data: {roles: ['user', 'admin']}},
  { path: 'bpm', component: BpmComponent, canActivate: [AuthGuard], data: {roles: ['user', 'admin']} },
  { path: 'bpm/modeling', component: BpmModelingComponent, canActivate: [AuthGuard], data: {roles: ['admin']} },
  { path: 'bpm/modeling/activity', component: ActivityComponent, canActivate: [AuthGuard], data: {roles: ['admin']} },
  { path: 'bpm/modeling/process', component: ProcessComponent, canActivate: [AuthGuard], data: {roles: ['admin']} },
  { path: 'bpm/execution', component: BpmExecutionComponent, canActivate: [AuthGuard], data: {roles: ['user', 'admin']} },
  { path: 'filter/modeling/all', component: FilterModelingComponent, canActivate: [AuthGuard], data: {roles: ['admin']} },
  { path: 'attribute', component: AttributeComponent, canActivate: [AuthGuard], data: {roles: ['admin']}  },
  { path: 'attribute/modeling', component: AttributeModelingComponent, canActivate: [AuthGuard], data: {roles: ['admin']}  },
  { path: 'modeling/attribute', component: AttributeModelingComponent, canActivate: [AuthGuard], data: {roles: ['admin']}  },
  { path: 'rating/modeling', component: RatingModelingBaseComponent, canActivate: [AuthGuard], data: {roles: ['admin']}  },
  { path: 'modeling/ontology', component: OntologyBaseComponent, canActivate: [AuthGuard], data: {roles: ['admin']}  },
  { path: 'modeling/users', component: UserComponent, canActivate: [AuthGuard], data: {roles: ['admin']} },
  { path: 'error', component: ErrorComponent },
  { path: 'access-denied', component: AccessDeniedComponent},
  { path: '**', redirectTo: 'error' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
