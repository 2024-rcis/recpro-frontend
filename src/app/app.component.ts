import {Component, ViewChild} from '@angular/core';
import {AlertComponent} from "./layout/alert/alert.component";
import {AlertType} from "./layout/alert/AlertType";
import {UserService} from "./components/user/services/user.service";
import {User} from "./model/User/User";
import {NgxSpinnerService} from "ngx-spinner";
import {LoaderService} from "./util/service/loader.service";
import {AuthService} from "./auth/auth.service";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  @ViewChild(AlertComponent, {static: true}) alertComponent: AlertComponent = new AlertComponent();
  collapsed = false;
  title = 'recpro-frontend';
  private user: User = new User();

  constructor(
    private userService: UserService,
    private spinner: NgxSpinnerService,
    private loaderService: LoaderService,
    private authService: AuthService,
    private httpClient: HttpClient
  ) {
    console.log(authService.getUsername());
    // this.userService.getCurrentUser().subscribe(res => this.user = res);
    // this.loaderService.get().subscribe(res => res ? spinner.show() : spinner.hide() );
  }

  addAlert() {
    this.alertComponent.addAlert('Test', AlertType.danger);
  }

  testRequest() {
    this.httpClient.get('http://localhost:8080/api/getAllTasks').subscribe(res => {
      console.log(res);
    })
  }
}
