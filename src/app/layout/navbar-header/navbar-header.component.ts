import { Component } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "../../components/user/services/user.service";
import {User} from "../../model/User/User";
import {AuthService} from "../../auth/auth.service";

@Component({
  selector: 'app-navbar-header',
  templateUrl: './navbar-header.component.html',
  styleUrls: ['./navbar-header.component.css']
})
export class NavbarHeaderComponent {

  user: User = new User();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService,
    private authService: AuthService
  ) {
    // this.userService.getCurrentUser().subscribe(res => this.user = res);

  }

  logout() {
    this.authService.logout();
  }

  isActiveRoute(route: string): boolean {
    return this.router.isActive(route, true);
  }
}
