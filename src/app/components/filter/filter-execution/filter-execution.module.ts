import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterExecutionComponent } from './filter-execution.component';
import { FilterExecutionAttributesComponent } from './filter-execution-attributes/filter-execution-attributes.component';
import { FilterExecutionMetaAttributesComponent } from './filter-execution-attributes/filter-execution-meta-attributes/filter-execution-meta-attributes.component';
import {MaterialModule} from "../../../material/material.module";
import {FormsModule} from "@angular/forms";



@NgModule({
  declarations: [
    FilterExecutionComponent,
    FilterExecutionAttributesComponent,
    FilterExecutionMetaAttributesComponent
  ],
  exports: [
    FilterExecutionMetaAttributesComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule
  ]
})
export class FilterExecutionModule { }
