import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterExecutionComponent } from './filter-execution.component';

describe('FilterExecutionComponent', () => {
  let component: FilterExecutionComponent;
  let fixture: ComponentFixture<FilterExecutionComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FilterExecutionComponent]
    });
    fixture = TestBed.createComponent(FilterExecutionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
