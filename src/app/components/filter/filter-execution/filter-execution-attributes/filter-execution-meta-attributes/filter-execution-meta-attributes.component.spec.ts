import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterExecutionMetaAttributesComponent } from './filter-execution-meta-attributes.component';

describe('FilterExecutionMetaAttributesComponent', () => {
  let component: FilterExecutionMetaAttributesComponent;
  let fixture: ComponentFixture<FilterExecutionMetaAttributesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FilterExecutionMetaAttributesComponent]
    });
    fixture = TestBed.createComponent(FilterExecutionMetaAttributesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
