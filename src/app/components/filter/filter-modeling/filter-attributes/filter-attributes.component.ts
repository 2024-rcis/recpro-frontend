import {Component, EventEmitter, Input, Output} from '@angular/core';
import {AttributeModelingService} from "../../../attribute/attribute-modeling/service/attribute-modeling.service";
import {RecproAttribute} from "../../../../model/RecPro/attributes/modeling/RecproAttribute";
import {RecproAttributeSelected} from "../model/RecproAttributeSelected";

@Component({
  selector: 'app-filter-attributes',
  templateUrl: './filter-attributes.component.html',
  styleUrls: ['./filter-attributes.component.css']
})
export class FilterAttributesComponent {

  @Input() filterAttributes: RecproAttribute[] = [];
  @Output() filterAttributesChange = new EventEmitter<RecproAttribute[]>;

  internalFilterAttributes: RecproAttributeSelected[] = [];
  availableAttributes: RecproAttributeSelected[] = [];

  selectedAttributeTypes = {
    meta: false,
    binary: false,
    textual: false,
    numerical: false
  }

  constructor(
    private attributeModelingService: AttributeModelingService
  ) {
    this.attributeModelingService.getAll().subscribe(res => {
      this.internalFilterAttributes = [];
      this.availableAttributes = res.map(attr =>  new RecproAttributeSelected(attr, false));
      this.filterAttributes.forEach(attr => {
        const avAttr = this.availableAttributes.find((a) => a.attribute.id === attr.id);
        if (avAttr) {
          avAttr.selected = true;
        }
      });
    });
  }

  onInternalFilterAttributesChange() {
    this.filterAttributes = this.internalFilterAttributes.map((filterAttr) => filterAttr.attribute);
    this.filterAttributesChange.emit(this.filterAttributes);
  }

}
