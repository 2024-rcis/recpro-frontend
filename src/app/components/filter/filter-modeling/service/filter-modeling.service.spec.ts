import { TestBed } from '@angular/core/testing';

import { FilterModelingService } from './filter-modeling.service';

describe('FilterModelingService', () => {
  let service: FilterModelingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FilterModelingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
