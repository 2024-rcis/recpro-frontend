import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HybridFilterModelingDetailComponent } from './hybrid-filter-modeling-detail.component';

describe('HybridFilterModelingDetailComponent', () => {
  let component: HybridFilterModelingDetailComponent;
  let fixture: ComponentFixture<HybridFilterModelingDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [HybridFilterModelingDetailComponent]
    });
    fixture = TestBed.createComponent(HybridFilterModelingDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
