import {Component, Input} from '@angular/core';
import {RecproHybridFilter} from "../../../../../model/RecPro/filter/RecproHybridFilter";

@Component({
  selector: 'app-hybrid-filter-modeling-detail',
  templateUrl: './hybrid-filter-modeling-detail.component.html',
  styleUrls: ['./hybrid-filter-modeling-detail.component.css']
})
export class HybridFilterModelingDetailComponent {

  @Input() filter: RecproHybridFilter = new RecproHybridFilter();
}
