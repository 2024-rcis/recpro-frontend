import {Component, Input} from '@angular/core';
import {RecproKnowledgeBasedFilter} from "../../../../../model/RecPro/filter/RecproKnowledgeBasedFilter";

@Component({
  selector: 'app-knowledge-based-filter-modeling-detail',
  templateUrl: './knowledge-based-filter-modeling-detail.component.html',
  styleUrls: ['./knowledge-based-filter-modeling-detail.component.css']
})
export class KnowledgeBasedFilterModelingDetailComponent {

  @Input() filter: RecproKnowledgeBasedFilter = new RecproKnowledgeBasedFilter();

}
