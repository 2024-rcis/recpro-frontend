import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddFilterModelingDialogComponent } from './add-filter-modeling-dialog.component';

describe('AddFilterModelingDialogComponent', () => {
  let component: AddFilterModelingDialogComponent;
  let fixture: ComponentFixture<AddFilterModelingDialogComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AddFilterModelingDialogComponent]
    });
    fixture = TestBed.createComponent(AddFilterModelingDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
