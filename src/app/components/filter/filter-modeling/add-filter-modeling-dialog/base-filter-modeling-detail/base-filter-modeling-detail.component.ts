import {Component, Input} from '@angular/core';
import {RecproBaseFilter} from "../../../../../model/RecPro/filter/RecproBaseFilter";
import {TasklistOrder} from "../../../../../model/RecPro/filter/TasklistOrder";

@Component({
  selector: 'app-base-filter-modeling-detail',
  templateUrl: './base-filter-modeling-detail.component.html',
  styleUrls: ['./base-filter-modeling-detail.component.css']
})
export class BaseFilterModelingDetailComponent {

  @Input() filter: RecproBaseFilter = new RecproBaseFilter();

  typeOptions = Object.values(TasklistOrder);

}
