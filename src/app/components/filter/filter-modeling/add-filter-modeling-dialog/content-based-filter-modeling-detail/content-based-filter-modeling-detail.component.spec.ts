import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentBasedFilterModelingDetailComponent } from './content-based-filter-modeling-detail.component';

describe('ContentBasedFilterModelingDetailComponent', () => {
  let component: ContentBasedFilterModelingDetailComponent;
  let fixture: ComponentFixture<ContentBasedFilterModelingDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ContentBasedFilterModelingDetailComponent]
    });
    fixture = TestBed.createComponent(ContentBasedFilterModelingDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
