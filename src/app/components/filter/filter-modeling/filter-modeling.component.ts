import {Component, OnInit} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {FilterModelingService} from "./service/filter-modeling.service";
import {RecproFilter} from "../../../model/RecPro/filter/RecproFilter";
import {AddFilterModelingDialogComponent} from "./add-filter-modeling-dialog/add-filter-modeling-dialog.component";

@Component({
  selector: 'app-filter-modeling',
  templateUrl: './filter-modeling.component.html',
  styleUrls: ['./filter-modeling.component.css']
})
export class FilterModelingComponent implements OnInit{

  filters: RecproFilter[] = [];

  constructor(
    private addFilterDialog: MatDialog,
    private service: FilterModelingService
  ) {
  }

  ngOnInit() {
    this.service.getAll().subscribe(res => {
      this.filters = res;
    });
  }

  addFilter() {
    this.addFilterDialog.open(
      AddFilterModelingDialogComponent,
      {
        enterAnimationDuration: 200,
        exitAnimationDuration: 200,
        closeOnNavigation: false,
        disableClose: true,
        // minHeight: 800,
        maxHeight: 800,
        minWidth: 800,
        maxWidth: 800
      }
    )
  }

  deleteFilter(filter: RecproFilter) {
    this.service.delete(filter.id);
  }

  editFilter(filter: RecproFilter) {
    this.addFilterDialog.open(
      AddFilterModelingDialogComponent,
      {
        enterAnimationDuration: 200,
        exitAnimationDuration: 200,
        data: filter,
        minHeight: 800,
        minWidth: 800,
        maxHeight: 800,
        maxWidth: 800,
        closeOnNavigation: false,
        disableClose: true
      }
    )
  }


}
