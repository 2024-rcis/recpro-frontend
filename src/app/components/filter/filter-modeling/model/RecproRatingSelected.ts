import {Rating} from "../../../../model/RecPro/rating/modelig/Rating";

export class RecproRatingSelected {
  rating: Rating = new Rating();
  selected: boolean = false;

  constructor(rating: Rating, selected: boolean) {
    this.rating = rating;
    this.selected = selected;
  }
}
