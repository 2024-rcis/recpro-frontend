import {Injectable} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {User} from "../../model/User/User";

@Injectable()
export class UserInterceptor implements HttpInterceptor {
  constructor(
  ) {
  }

  private user: User = new User();

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let storageUserId = localStorage.getItem('userID');
    let userId = '31dea2b6-7de1-47e4-b5cb-ef4eda5549c1';

    if (storageUserId != null) {
      userId = storageUserId;
    }

    if (this.user) {
      req = req.clone({
        setHeaders: {
          'X-User-ID': userId,
        },
      })
    }

    return next.handle(req);
  }
}
