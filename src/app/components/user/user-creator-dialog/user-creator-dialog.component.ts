import {Component, Inject} from '@angular/core';
import {User} from "../../../model/User/User";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {UserModelingService} from "../services/user-modeling.service";

@Component({
  selector: 'app-user-creator-dialog',
  templateUrl: './user-creator-dialog.component.html',
  styleUrls: ['./user-creator-dialog.component.css']
})
export class UserCreatorDialogComponent {

  user: User = new User();

  constructor(
    private dialogRef: MatDialogRef<UserCreatorDialogComponent>,
    private userModelingService: UserModelingService,
    @Inject(MAT_DIALOG_DATA) public userData: User
  ) {
    if (this.userData) {
      this.user = this.userData;
    }
  }

  cancel() {
    this.dialogRef.close();
  }

  save() {
    this.userModelingService.create(this.user);
    this.dialogRef.close();
  }

}
