import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "../../../../util/config.service";
import {RatingDto} from "../../../../model/RecPro/rating/dto/RatingDto";
import {Observable} from "rxjs";
import {RatingInstance} from "../../../../model/RecPro/rating/execution/RatingInstance";

@Injectable({
  providedIn: 'root'
})
export class RatingExecutionService {

  constructor(
    private http: HttpClient,
    private configService: ConfigService
  ) { }

  getAll(): Observable<RatingInstance[]> {
    return this.http.get<RatingInstance[]>(this.configService.apiConfig.recpro.rating.persistence.getAll());
  }

  init(bpmElementId: string, elementInstanceId: string, processInstanceId: string): Observable<RatingDto> {
    return this.http.get<RatingDto>(this.configService.apiConfig.recpro.rating.execution.init(bpmElementId, elementInstanceId, processInstanceId));
  }

  save(instance: RatingInstance) {
    return this.http.post(this.configService.apiConfig.recpro.rating.persistence.save(), instance);
  }

}
