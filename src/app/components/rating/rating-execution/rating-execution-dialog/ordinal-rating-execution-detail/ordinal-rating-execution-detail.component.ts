import {Component, Input} from '@angular/core';
import {OrdinalRating} from "../../../../../model/RecPro/rating/modelig/OrdinalRating";
import {OrdinalRatingInstance} from "../../../../../model/RecPro/rating/execution/OrdinalRatingInstance";

@Component({
  selector: 'app-ordinal-rating-execution-detail',
  templateUrl: './ordinal-rating-execution-detail.component.html',
  styleUrls: ['./ordinal-rating-execution-detail.component.css']
})
export class OrdinalRatingExecutionDetailComponent {
  @Input() ordinalRating: OrdinalRating = new OrdinalRating();
  @Input() ordinalRatingInstance: OrdinalRatingInstance = new OrdinalRatingInstance();
}
