import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdinalRatingExecutionDetailComponent } from './ordinal-rating-execution-detail.component';

describe('OrdinalRatingExecutionDetailComponent', () => {
  let component: OrdinalRatingExecutionDetailComponent;
  let fixture: ComponentFixture<OrdinalRatingExecutionDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [OrdinalRatingExecutionDetailComponent]
    });
    fixture = TestBed.createComponent(OrdinalRatingExecutionDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
