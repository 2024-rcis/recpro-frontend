import {Component, Input, OnInit} from '@angular/core';
import {IntervalBasedRating} from "../../../../../model/RecPro/rating/modelig/IntervalBasedRating";
import {IntervalBasedRatingInstance} from "../../../../../model/RecPro/rating/execution/IntervalBasedRatingInstance";

@Component({
  selector: 'app-interval-based-rating-execution-detail',
  templateUrl: './interval-based-rating-execution-detail.component.html',
  styleUrls: ['./interval-based-rating-execution-detail.component.css']
})
export class IntervalBasedRatingExecutionDetailComponent implements OnInit {

  @Input() intervalBasedRating: IntervalBasedRating = new IntervalBasedRating();
  @Input() intervalBasedRatingInstance: IntervalBasedRatingInstance = new IntervalBasedRatingInstance();

  ngOnInit() {
    this.intervalBasedRating.scale.details.sort((a, b) => {
      if (a.position < b.position) {
        return -1;
      } else if(a.position > b.position) {
        return 1;
      } else {
        return 0;
      }
    });
  }
}
