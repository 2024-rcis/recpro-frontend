import {Component, Input} from '@angular/core';
import {UnaryRating} from "../../../../../model/RecPro/rating/modelig/UnaryRating";
import {UnaryRatingInstance} from "../../../../../model/RecPro/rating/execution/UnaryRatingInstance";

@Component({
  selector: 'app-unary-rating-execution-detail',
  templateUrl: './unary-rating-execution-detail.component.html',
  styleUrls: ['./unary-rating-execution-detail.component.css']
})
export class UnaryRatingExecutionDetailComponent {
  @Input() unaryRating: UnaryRating = new UnaryRating();
  @Input() unaryRatingInstance: UnaryRatingInstance = new UnaryRatingInstance();
}
