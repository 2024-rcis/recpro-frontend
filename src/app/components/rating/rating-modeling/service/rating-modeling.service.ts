import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "../../../../util/config.service";
import {BehaviorSubject, Observable} from "rxjs";
import {Rating} from "../../../../model/RecPro/rating/modelig/Rating";
import {LoaderService} from "../../../../util/service/loader.service";

@Injectable({
  providedIn: 'root'
})
export class RatingModelingService {

  private ratings: BehaviorSubject<Rating[]> = new BehaviorSubject<Rating[]>([]);
  private defaultRating: BehaviorSubject<Rating> = new BehaviorSubject<Rating>(new Rating());

  constructor(
    private http: HttpClient,
    private configService: ConfigService,
    private loaderService: LoaderService
  ) { }

  getAll() {
    this.loaderService.load();
    this.http.get<Rating[]>(this.configService.apiConfig.modeling.rating.getAll()).subscribe(res => {
      this.setRatings(res);
      this.loaderService.finish();
    });
    return this.getRatings();
  }

  setRatings(ratings: Rating[]) {
    this.ratings.next(ratings);
  }

  getRatings(): Observable<Rating[]> {
    return this.ratings.asObservable();
  }

  create(rating: Rating) {
    this.loaderService.load();
    this.http.post(this.configService.apiConfig.modeling.rating.create(), rating).subscribe(() => {
      this.getAll();
      this.loaderService.finish();
    });
  }

  delete(ratingId: string) {
    this.loaderService.load();
    this.http.delete(this.configService.apiConfig.modeling.rating.delete(ratingId)).subscribe(() => {
      this.getAll();
      this.loaderService.finish();
    });
  }

  setDefaultRating(ratingId: string) {
    this.loaderService.load();
    this.http.get(this.configService.apiConfig.modeling.rating.setDefaultRating(ratingId)).subscribe(() => {
      this.getAll();
      this.loaderService.finish();
    });
  }

  getDefaultRating(): Observable<Rating> {
    this.loaderService.load();
    this.http.get<Rating>(this.configService.apiConfig.modeling.rating.getDefaultRating()).subscribe(res => {
      this.loaderService.finish();
      this.setDefaultRatingBehavior(res);
    });
    return this.getDefaultRatingBehavior();
  }

  private setDefaultRatingBehavior(rating: Rating) {
    this.defaultRating.next(rating);
  }

  private getDefaultRatingBehavior(): Observable<Rating> {
    return this.defaultRating.asObservable();
  }
}
