import { TestBed } from '@angular/core/testing';

import { RatingModelingService } from './rating-modeling.service';

describe('RatingModelingService', () => {
  let service: RatingModelingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RatingModelingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
