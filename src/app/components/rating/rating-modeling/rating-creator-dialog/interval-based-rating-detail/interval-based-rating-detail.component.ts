import {Component, Input} from '@angular/core';
import {IntervalBasedRating} from "../../../../../model/RecPro/rating/modelig/IntervalBasedRating";

@Component({
  selector: 'app-interval-based-rating-detail',
  templateUrl: './interval-based-rating-detail.component.html',
  styleUrls: ['./interval-based-rating-detail.component.css']
})
export class IntervalBasedRatingDetailComponent {
  @Input() rating: IntervalBasedRating = new IntervalBasedRating();
}
