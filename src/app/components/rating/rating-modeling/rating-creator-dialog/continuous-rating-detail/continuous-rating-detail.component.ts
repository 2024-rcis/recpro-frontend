import {Component, Input} from '@angular/core';
import {ContinuousRating} from "../../../../../model/RecPro/rating/modelig/ContinuousRating";

@Component({
  selector: 'app-continuous-rating-detail',
  templateUrl: './continuous-rating-detail.component.html',
  styleUrls: ['./continuous-rating-detail.component.css']
})
export class ContinuousRatingDetailComponent {
  @Input() rating: ContinuousRating = new ContinuousRating();
}
