import {Component, Input} from '@angular/core';
import {OrdinalRating} from "../../../../../model/RecPro/rating/modelig/OrdinalRating";

@Component({
  selector: 'app-ordinal-rating-detail',
  templateUrl: './ordinal-rating-detail.component.html',
  styleUrls: ['./ordinal-rating-detail.component.css']
})
export class OrdinalRatingDetailComponent {
  @Input() rating: OrdinalRating = new OrdinalRating();
}
