import {Component, OnInit} from '@angular/core';
import {RatingModelingService} from "../service/rating-modeling.service";
import {Rating} from "../../../../model/RecPro/rating/modelig/Rating";
import {MatDialog} from "@angular/material/dialog";
import {RatingCreatorDialogComponent} from "../rating-creator-dialog/rating-creator-dialog.component";
@Component({
  selector: 'app-rating-modeling-base',
  templateUrl: './rating-modeling-base.component.html',
  styleUrls: ['./rating-modeling-base.component.css']
})
export class RatingModelingBaseComponent implements OnInit {

  ratings: Rating[] = [];
  defaultRating: Rating = new Rating();

  constructor(
    private ratingModelingService: RatingModelingService,
    private addRatingDialog: MatDialog
  ) {
  }

  ngOnInit(): void {
    this.ratingModelingService.getAll().subscribe(res => {
      this.ratings = res;
    });

    this.ratingModelingService.getDefaultRating().subscribe(res => this.defaultRating = res);
  }

  editRating(rating: Rating) {
    this.addRatingDialog.open(
      RatingCreatorDialogComponent,
      {
        enterAnimationDuration: 200,
        exitAnimationDuration: 200,
        data: rating,
        minHeight: 800,
        minWidth: 800,
        maxHeight: 800,
        maxWidth: 800,
        closeOnNavigation: false,
        disableClose: true
      }
    );
  }

  deleteRating(rating: Rating) {
    this.ratingModelingService.delete(rating.id);
  }

  addRating() {
    this.addRatingDialog.open(
      RatingCreatorDialogComponent,
      {
        enterAnimationDuration: 200,
        exitAnimationDuration: 200,
        closeOnNavigation: false,
        disableClose: true,
        // minHeight: 800,
        maxHeight: 800,
        minWidth: 800,
        maxWidth: 800
      }
    )
  }

  setDefaultRating(ratingId: string) {
    this.ratingModelingService.setDefaultRating(ratingId);
  }
}
