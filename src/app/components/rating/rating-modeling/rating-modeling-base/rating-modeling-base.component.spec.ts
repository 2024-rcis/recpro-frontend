import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RatingModelingBaseComponent } from './rating-modeling-base.component';

describe('RatingModelingBaseComponent', () => {
  let component: RatingModelingBaseComponent;
  let fixture: ComponentFixture<RatingModelingBaseComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RatingModelingBaseComponent]
    });
    fixture = TestBed.createComponent(RatingModelingBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
