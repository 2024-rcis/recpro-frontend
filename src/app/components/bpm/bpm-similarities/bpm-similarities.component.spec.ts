import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BpmSimilaritiesComponent } from './bpm-similarities.component';

describe('BpmSimilaritiesComponent', () => {
  let component: BpmSimilaritiesComponent;
  let fixture: ComponentFixture<BpmSimilaritiesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BpmSimilaritiesComponent]
    });
    fixture = TestBed.createComponent(BpmSimilaritiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
