import { Injectable } from '@angular/core';
import {ConfigService} from "../../../../util/config.service";

@Injectable({
  providedIn: 'root'
})
export class BpmSimilaritiesService {

  constructor(
    private http: HttpClient,
    private config: ConfigService
  ) { }
}
