import { TestBed } from '@angular/core/testing';

import { BpmSimilaritiesService } from './bpm-similarities.service';

describe('BpmSimilaritiesService', () => {
  let service: BpmSimilaritiesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BpmSimilaritiesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
