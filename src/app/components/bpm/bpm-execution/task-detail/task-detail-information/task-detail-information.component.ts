import {Component} from '@angular/core';
import {TaskDetailComponent} from "../task-detail.component";

@Component({
  selector: 'app-task-detail-information',
  templateUrl: './task-detail-information.component.html',
  styleUrls: ['./task-detail-information.component.css']
})
export class TaskDetailInformationComponent extends TaskDetailComponent {
}
