import { Component } from '@angular/core';
import {TaskDetailComponent} from "../task-detail.component";

@Component({
  selector: 'app-task-detail-description',
  templateUrl: './task-detail-description.component.html',
  styleUrls: ['./task-detail-description.component.css']
})
export class TaskDetailDescriptionComponent extends TaskDetailComponent {

}
