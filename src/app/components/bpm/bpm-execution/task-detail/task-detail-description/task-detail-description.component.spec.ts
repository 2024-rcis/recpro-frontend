import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskDetailDescriptionComponent } from './task-detail-description.component';

describe('TaskDetailDescriptionComponent', () => {
  let component: TaskDetailDescriptionComponent;
  let fixture: ComponentFixture<TaskDetailDescriptionComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TaskDetailDescriptionComponent]
    });
    fixture = TestBed.createComponent(TaskDetailDescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
