import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskDetailProcessComponent } from './task-detail-process.component';

describe('TaskDetailProcessComponent', () => {
  let component: TaskDetailProcessComponent;
  let fixture: ComponentFixture<TaskDetailProcessComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TaskDetailProcessComponent]
    });
    fixture = TestBed.createComponent(TaskDetailProcessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
