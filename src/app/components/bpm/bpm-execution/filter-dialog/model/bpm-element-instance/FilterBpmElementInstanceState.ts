export enum FilterBpmElementInstanceState {
  TRUE = 'TRUE',
  FALSE = 'FALSE',
  NEUTRAL = 'NEUTRAL'
}
