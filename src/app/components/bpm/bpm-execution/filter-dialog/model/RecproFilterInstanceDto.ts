import {RecproFilter} from "../../../../../model/RecPro/filter/RecproFilter";
import {RecproFilterInstance} from "./RecproFilterInstance";

export class RecproFilterInstanceDto {
  filterInstance: RecproFilterInstance;
  filter: RecproFilter;

  constructor() {
    this.filter = new RecproFilter();
    this.filterInstance = new RecproFilterInstance();
  }
}
