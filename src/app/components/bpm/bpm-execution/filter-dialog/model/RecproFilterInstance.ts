import {FilterType} from "../../../../../model/RecPro/filter/FilterType";
import {TasklistOrder} from "../../../../../model/BPM/execution/TasklistOrder";
import {FilterResult} from "./result/FilterResult";

export class RecproFilterInstance {
  id: number;
  filterId: string;
  tasklistId: number;
  assigneeId: string;
  filterType: FilterType;
  considerProcessAttributes: boolean;
  ascending: boolean;
  tasklistOrder: TasklistOrder;
  result: FilterResult;

  constructor() {
    this.id = -1;
    this.filterId = '';
    this.filterType = FilterType.KNOWLEDGE_BASED;
    this.considerProcessAttributes = true;
    this.ascending = true;
    this.tasklistOrder = TasklistOrder.CREATE_DATE;
    this.tasklistId = -1;
    this.assigneeId = '';
    this.result = new FilterResult();
  }
}
