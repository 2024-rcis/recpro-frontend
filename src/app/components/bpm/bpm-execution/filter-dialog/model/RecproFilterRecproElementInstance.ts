import {RecproFilterRecproElementState} from "./RecproFilterRecproElementState";

export class RecproFilterRecproElementInstance {
  id: number;
  recproElementId: string;
  state: RecproFilterRecproElementState;
  constructor() {
    this.id = -1;
    this.recproElementId = '';
    this.state = RecproFilterRecproElementState.NEUTRAL;
  }
}
