import {Component, OnInit} from '@angular/core';
import {RecproAttribute} from "../../../../../model/RecPro/attributes/modeling/RecproAttribute";
import {RecproFilterAttributeInstance} from "../model/attribute/RecproFilterAttributeInstance";
import {RecproMetaAttribute} from "../../../../../model/RecPro/attributes/modeling/RecproMetaAttribute";
import {RecproFilterMetaAttributeInstance} from "../model/attribute/RecproFilterMetaAttributeInstance";
import {RecproAttributeType} from "../../../../../model/RecPro/attributes/modeling/RecproAttributeType";
import {TasklistService} from "../../services/tasklist.service";
import {RecproKnowledgeBasedFilter} from "../../../../../model/RecPro/filter/RecproKnowledgeBasedFilter";
import {RecproKnowledgeBasedFilterInstance} from "../model/RecproKnowledgeBasedFilterInstance";

@Component({
  selector: 'app-filter-execution-attributes',
  templateUrl: './filter-execution-attributes.component.html',
  styleUrls: ['./filter-execution-attributes.component.css']
})
export class FilterExecutionAttributesComponent implements OnInit {

  attributes: RecproAttribute[] = [];
  attributeInstances: RecproFilterAttributeInstance[] = [];

  constructor(
    private tasklistService: TasklistService
  ) {
  }

  ngOnInit() {
    this.tasklistService.getTaskListObservable().subscribe(res => {
      this.attributes = (res.knowledgeBasedFilter.filter as RecproKnowledgeBasedFilter).attributes;
      this.attributeInstances = (res.knowledgeBasedFilter.filterInstance as RecproKnowledgeBasedFilterInstance).attributes;
    });
  }

  getAttributeType(attribute: RecproAttribute): RecproAttributeType {
    return attribute.attributeType;
  }

  getRecproMetaAttribute(attribute: RecproAttribute): RecproMetaAttribute {
    return attribute as RecproMetaAttribute;
  }

  getRecproFilterMetaAttributeInstance(attribute: RecproAttribute): RecproFilterMetaAttributeInstance {
    return  this.attributeInstances.filter(instance => {
      return instance.attributeId === attribute.id
    }).at(0) as RecproFilterMetaAttributeInstance;
  }

  log() {
    console.log(this.attributeInstances);
    console.log(this.attributes);
  }

}
