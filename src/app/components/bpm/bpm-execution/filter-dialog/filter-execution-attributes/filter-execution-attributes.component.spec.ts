import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterExecutionAttributesComponent } from './filter-execution-attributes.component';

describe('FilterExecutionAttributesComponent', () => {
  let component: FilterExecutionAttributesComponent;
  let fixture: ComponentFixture<FilterExecutionAttributesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FilterExecutionAttributesComponent]
    });
    fixture = TestBed.createComponent(FilterExecutionAttributesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
