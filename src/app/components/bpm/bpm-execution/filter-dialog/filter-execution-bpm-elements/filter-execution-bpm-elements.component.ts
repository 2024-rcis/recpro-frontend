import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {RecproElement} from "../../../../../model/BPM/modeling/RecproElement";
import {RecproFilterRecproElementInstance} from "../model/RecproFilterRecproElementInstance";
import {RecproElementType} from "../../../../../model/BPM/modeling/RecproElementType";

@Component({
  selector: 'app-filter-execution-bpm-elements',
  templateUrl: './filter-execution-bpm-elements.component.html',
  styleUrls: ['./filter-execution-bpm-elements.component.css']
})
export class FilterExecutionBpmElementsComponent implements OnChanges {

  @Input() bpmElements: RecproElement[] = [];
  @Input() bpmElementInstances: RecproFilterRecproElementInstance[] = [];


  ngOnChanges(changes: SimpleChanges) {

  }

  log() {
    console.log(this.bpmElements);
    console.log(this.bpmElementInstances);
  }

  getElementType(element: RecproElement): RecproElementType {
    return element.elementType;
  }

  getBpmElement(element: RecproElement): RecproElement {
    return element;
  }

  getRecproFilterRecproElementInstance(element: RecproElement): RecproFilterRecproElementInstance {
    return  this.bpmElementInstances.filter(instance => {
      return instance.recproElementId === element.id
    }).at(0) as RecproFilterRecproElementInstance;
    // return instance as RecproFilterMetaAttributeInstance;
  }



  isElementType(type: string, element: RecproElement) {
    return element.elementType === type;
  }
}
