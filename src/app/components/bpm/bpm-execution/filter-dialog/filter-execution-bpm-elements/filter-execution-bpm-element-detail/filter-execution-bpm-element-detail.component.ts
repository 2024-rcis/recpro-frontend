import {Component, Input} from '@angular/core';
import {RecproElement} from "../../../../../../model/BPM/modeling/RecproElement";
import {RecproFilterRecproElementInstance} from "../../model/RecproFilterRecproElementInstance";
import {RecproFilterRecproElementState} from "../../model/RecproFilterRecproElementState";

@Component({
  selector: 'app-filter-execution-bpm-element-detail',
  templateUrl: './filter-execution-bpm-element-detail.component.html',
  styleUrls: ['./filter-execution-bpm-element-detail.component.css']
})
export class FilterExecutionBpmElementDetailComponent {

  @Input() bpmElement: RecproElement = new RecproElement();
  @Input() instance: RecproFilterRecproElementInstance = new RecproFilterRecproElementInstance();

  changeElementStatus(instance: RecproFilterRecproElementInstance) {
    switch (this.instance.state) {
      case RecproFilterRecproElementState.TRUE: this.instance.state = RecproFilterRecproElementState.FALSE;
        break;
      case RecproFilterRecproElementState.FALSE: this.instance.state = RecproFilterRecproElementState.NEUTRAL;
        break;
      default: this.instance.state = RecproFilterRecproElementState.TRUE;
        break;
    }
  }

}
