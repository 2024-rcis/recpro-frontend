import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterExecutionBpmElementsComponent } from './filter-execution-bpm-elements.component';

describe('FilterExecutionBpmElementsComponent', () => {
  let component: FilterExecutionBpmElementsComponent;
  let fixture: ComponentFixture<FilterExecutionBpmElementsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FilterExecutionBpmElementsComponent]
    });
    fixture = TestBed.createComponent(FilterExecutionBpmElementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
