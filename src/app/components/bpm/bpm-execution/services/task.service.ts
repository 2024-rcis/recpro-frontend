import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "../../../../util/config.service";
import {BehaviorSubject, Observable} from "rxjs";
import {RecproTaskDto} from "../../../../model/BPM/execution/RecproTaskDto";
import {TasklistService} from "./tasklist.service";
import {LoaderService} from "../../../../util/service/loader.service";

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  private currentTask: BehaviorSubject<RecproTaskDto> = new BehaviorSubject<RecproTaskDto>(new RecproTaskDto());
  private executedTask: BehaviorSubject<RecproTaskDto> = new BehaviorSubject<RecproTaskDto>(new RecproTaskDto());

  constructor(
    private http: HttpClient,
    private configService: ConfigService,
    private tasklistService: TasklistService,
    private loaderService: LoaderService
  ) {
  }

  claimTask(task: RecproTaskDto) {
    this.loaderService.load();
    this.http.post<RecproTaskDto>(this.configService.apiConfig.recpro.bpm.bpmExecutionApi.task.claimTask(), task).subscribe(res => {
      this.tasklistService.getTasklist();
      this.setCurrentTask(res);
    });
  }

  unclaimTask(task: RecproTaskDto) {
    this.loaderService.load();
    this.http.post<RecproTaskDto>(this.configService.apiConfig.recpro.bpm.bpmExecutionApi.task.unclaimTask(), task).subscribe(res => {
      this.tasklistService.getTasklist();
      this.setCurrentTask(res);
    });
  }

  completeTask(task: RecproTaskDto) {
    this.loaderService.load();
    this.http.post<RecproTaskDto>(this.configService.apiConfig.recpro.bpm.bpmExecutionApi.task.completeTask(), task).subscribe(() => {
      this.setCurrentTask(new RecproTaskDto());
      this.tasklistService.getTasklist();
    });
  }

  getCurrentTask() {
    return this.currentTask;
  }

  startTask(task: RecproTaskDto) {
    this.loaderService.load();
    this.http.post<RecproTaskDto>(this.configService.apiConfig.recpro.bpm.bpmExecutionApi.task.startTask(), task).subscribe(res => {
      this.tasklistService.getTasklist();
      this.setCurrentTask(res);
    });
  }

  stopTask(task: RecproTaskDto) {
    this.loaderService.load();
    this.http.post<RecproTaskDto>(this.configService.apiConfig.recpro.bpm.bpmExecutionApi.task.stopTask(), task).subscribe(res => {
      this.tasklistService.getTasklist();
      this.setCurrentTask(res);
    });
  }

  setCurrentTask(task: RecproTaskDto) {
    this.currentTask.next(task);
    this.loaderService.finish();
  }

  getLatestExecuted(): Observable<RecproTaskDto> {
    this.loaderService.load();
    this.http.get<RecproTaskDto>(this.configService.apiConfig.recpro.bpm.bpmExecutionApi.task.getLatestByUserId()).subscribe(res => {
      this.setLatestExecuted(res);
    });
    return this.executedTask.asObservable();
  }

  setLatestExecuted(task: RecproTaskDto) {
    this.executedTask.next(task);
    this.loaderService.finish();
  }
}
