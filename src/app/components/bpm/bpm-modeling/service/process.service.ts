import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject, Observable} from "rxjs";
import {ConfigService} from "../../../../util/config.service";
import {RecproProcess} from "../../../../model/BPM/modeling/RecproProcess";
import {LoaderService} from "../../../../util/service/loader.service";

@Injectable({
  providedIn: 'root'
})
export class ProcessService {

  private processes: BehaviorSubject<RecproProcess[]> = new BehaviorSubject<RecproProcess[]>([]);

  constructor(
    private httpClient: HttpClient,
    private configService: ConfigService,
    private loaderService: LoaderService
  ) { }

  getAll(): Observable<RecproProcess[]> {
    this.loaderService.load();
    this.httpClient.get<RecproProcess[]>(this.configService.apiConfig.modeling.bpm.process.getAll()).subscribe(res => {
      this.set(res);
      this.loaderService.finish();
    });
    return this.get();
  }

  getLatest(): Observable<RecproProcess[]> {
    return this.httpClient.get<RecproProcess[]>(this.configService.apiConfig.modeling.bpm.process.getLatest());
  }

  createAll() {
    this.loaderService.load();
    this.httpClient.get(this.configService.apiConfig.modeling.bpm.process.createAll()).subscribe(() => this.loaderService.finish());
  }

  get(): Observable<RecproProcess[]> {
    return this.processes.asObservable();
  }

  set(processes: RecproProcess[]) {
    this.processes.next(processes);
  }
}
