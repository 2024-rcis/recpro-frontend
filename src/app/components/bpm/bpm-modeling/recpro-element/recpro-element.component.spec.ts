import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecproElementComponent } from './recpro-element.component';

describe('RecproElementComponent', () => {
  let component: RecproElementComponent;
  let fixture: ComponentFixture<RecproElementComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RecproElementComponent]
    });
    fixture = TestBed.createComponent(RecproElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
