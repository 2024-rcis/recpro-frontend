import {Component, OnInit} from '@angular/core';
import {RecproProcess} from "../../../../../model/BPM/modeling/RecproProcess";
import {RecproProcessModel} from "../../../../../model/BPM/modeling/RecproProcessModel";
import {RecproElementComponent} from "../recpro-element.component";

@Component({
  selector: 'app-process',
  templateUrl: './process.component.html',
  styleUrls: ['./process.component.css']
})
export class ProcessComponent extends RecproElementComponent implements OnInit{
  processes: RecproProcess[] = [];

  ngOnInit() {
    this.processService.getAll().subscribe(res => {
      this.processes = res;
      console.log(res);
    });
  }

  openProcessModel(processModel: RecproProcessModel) {
    console.log(processModel);
  }
}
