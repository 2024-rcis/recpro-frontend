import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BpmComponent} from "./bpm.component";
import {BpmModelingComponent} from "./bpm-modeling/bpm-modeling.component";
import {BpmExecutionComponent} from "./bpm-execution/bpm-execution.component";
import { TasklistComponent } from './bpm-execution/tasklist/tasklist.component';
import { TaskDetailComponent } from './bpm-execution/task-detail/task-detail.component';
import { TasklistElementComponent } from './bpm-execution/tasklist/tasklist-element/tasklist-element.component';
import {ClarityModule} from "@clr/angular";
import {MaterialModule} from "../../material/material.module";
import { TaskDetailDescriptionComponent } from './bpm-execution/task-detail/task-detail-description/task-detail-description.component';
import { TaskDetailFormComponent } from './bpm-execution/task-detail/task-detail-form/task-detail-form.component';
import { TaskDetailInformationComponent } from './bpm-execution/task-detail/task-detail-information/task-detail-information.component';
import { TaskDetailProcessComponent } from './bpm-execution/task-detail/task-detail-process/task-detail-process.component';
import {LayoutModule} from "../../layout/layout.module";
import {FormsModule} from "@angular/forms";
import { ActivityComponent } from './bpm-modeling/recpro-element/activity/activity.component';
import { ProcessComponent } from './bpm-modeling/recpro-element/process/process.component';
import {RouterModule} from "@angular/router";
import { RecproElementComponent } from './bpm-modeling/recpro-element/recpro-element.component';
import {AttributeModelingModule} from "../attribute/attribute-modeling/attribute-modeling.module";
import { StartProcessComponent } from './bpm-execution/start-process/start-process.component';
import { StartProcessDetailComponent } from './bpm-execution/start-process/start-process-detail/start-process-detail.component';
import {AttributeExecutionModule} from "../attribute/attribute-execution/attribute-execution.module";
import { FilterDialogComponent } from './bpm-execution/filter-dialog/filter-dialog.component';
import { FilterExecutionAttributesComponent } from './bpm-execution/filter-dialog/filter-execution-attributes/filter-execution-attributes.component';
import { FilterExecutionBpmElementsComponent } from './bpm-execution/filter-dialog/filter-execution-bpm-elements/filter-execution-bpm-elements.component';
import {FilterExecutionModule} from "../filter/filter-execution/filter-execution.module";
import { FilterExecutionBpmElementDetailComponent } from './bpm-execution/filter-dialog/filter-execution-bpm-elements/filter-execution-bpm-element-detail/filter-execution-bpm-element-detail.component';
import { BpmSimilaritiesComponent } from './bpm-similarities/bpm-similarities.component';



@NgModule({
  declarations: [
    BpmComponent,
    BpmModelingComponent,
    BpmExecutionComponent,
    TasklistComponent,
    TaskDetailComponent,
    TasklistElementComponent,
    TaskDetailDescriptionComponent,
    TaskDetailFormComponent,
    TaskDetailInformationComponent,
    TaskDetailProcessComponent,
    ActivityComponent,
    ProcessComponent,
    RecproElementComponent,
    StartProcessComponent,
    StartProcessDetailComponent,
    FilterDialogComponent,
    FilterExecutionAttributesComponent,
    FilterExecutionBpmElementsComponent,
    FilterExecutionBpmElementDetailComponent,
    BpmSimilaritiesComponent,
  ],
    imports: [
        CommonModule,
        ClarityModule,
        MaterialModule,
        AttributeExecutionModule,
        AttributeModelingModule,
        LayoutModule,
        FormsModule,
        RouterModule.forRoot([], {useHash: true}),
        FilterExecutionModule,
    ]
})
export class BpmModule { }
