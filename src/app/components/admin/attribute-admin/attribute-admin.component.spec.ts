import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AttributeAdminComponent } from './attribute-admin.component';

describe('AttributeAdminComponent', () => {
  let component: AttributeAdminComponent;
  let fixture: ComponentFixture<AttributeAdminComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AttributeAdminComponent]
    });
    fixture = TestBed.createComponent(AttributeAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
