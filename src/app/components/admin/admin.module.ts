import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { AttributeAdminComponent } from './attribute-admin/attribute-admin.component';



@NgModule({
  declarations: [
    AdminComponent,
    AttributeAdminComponent
  ],
  imports: [
    CommonModule
  ]
})
export class AdminModule { }
