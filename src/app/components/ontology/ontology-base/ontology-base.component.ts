import { Component } from '@angular/core';
import {OntologyService} from "../service/ontology.service";
import {ProcessService} from "../../bpm/bpm-modeling/service/process.service";

@Component({
  selector: 'app-ontology-base',
  templateUrl: './ontology-base.component.html',
  styleUrls: ['./ontology-base.component.css']
})
export class OntologyBaseComponent {

  constructor(
    private ontologyService: OntologyService,
    private processService: ProcessService
  ) {
  }

  clearOntology() {
    this.ontologyService.clearOntology();
  }

  createAllProcesses() {
    console.log('CREATE PROCESSES');
    this.processService.createAll();
  }

}
