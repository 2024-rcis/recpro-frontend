import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OntologyBaseComponent } from './ontology-base.component';

describe('OntologyBaseComponent', () => {
  let component: OntologyBaseComponent;
  let fixture: ComponentFixture<OntologyBaseComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [OntologyBaseComponent]
    });
    fixture = TestBed.createComponent(OntologyBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
