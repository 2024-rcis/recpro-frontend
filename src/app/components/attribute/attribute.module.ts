import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AttributeComponent } from './attribute.component';
import {ClarityModule, ClrCheckboxModule, ClrIconModule} from "@clr/angular";
import {FormsModule} from "@angular/forms";
import {MaterialModule} from "../../material/material.module";



@NgModule({
    declarations: [
        AttributeComponent,
    ],
    exports: [
        AttributeComponent,
    ],
  imports: [
    CommonModule,
    ClrCheckboxModule,
    ClrIconModule,
    ClarityModule,
    FormsModule,
    MaterialModule
  ]
})
export class AttributeModule { }
