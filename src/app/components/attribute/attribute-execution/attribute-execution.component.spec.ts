import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AttributeExecutionComponent } from './attribute-execution.component';

describe('AttributeExecutionComponent', () => {
  let component: AttributeExecutionComponent;
  let fixture: ComponentFixture<AttributeExecutionComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AttributeExecutionComponent]
    });
    fixture = TestBed.createComponent(AttributeExecutionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
