import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AttributeExecutionComponent} from "./attribute-execution.component";
import {AttributeDetailComponent} from "./attribute-detail/attribute-detail.component";
import { BinaryAttributeComponent } from './attribute-detail/binary-attribute/binary-attribute.component';
import { TextualAttributeComponent } from './attribute-detail/textual-attribute/textual-attribute.component';
import { NumericAttributeComponent } from './attribute-detail/numeric-attribute/numeric-attribute.component';
import { BaseAttributeComponent } from './attribute-detail/base-attribute/base-attribute.component';
import {MaterialModule} from "../../../material/material.module";
import {FormsModule} from "@angular/forms";
import {MatTooltipModule} from "@angular/material/tooltip";
import { MetaAttributeComponent } from './attribute-detail/meta-attribute/meta-attribute.component';



@NgModule({
  declarations: [
    AttributeExecutionComponent,
    AttributeDetailComponent,
    BinaryAttributeComponent,
    TextualAttributeComponent,
    NumericAttributeComponent,
    BaseAttributeComponent,
    MetaAttributeComponent
  ],
  exports: [
    AttributeExecutionComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    MatTooltipModule
  ]
})
export class AttributeExecutionModule { }
