import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BinaryAttributeComponent } from './binary-attribute.component';

describe('BinaryAttributeComponent', () => {
  let component: BinaryAttributeComponent;
  let fixture: ComponentFixture<BinaryAttributeComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BinaryAttributeComponent]
    });
    fixture = TestBed.createComponent(BinaryAttributeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
