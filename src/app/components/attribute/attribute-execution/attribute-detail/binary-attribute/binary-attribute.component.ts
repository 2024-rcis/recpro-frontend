import {Component, Input} from '@angular/core';
import {
  RecproBinaryAttributeInstance
} from "../../../../../model/RecPro/attributes/execution/RecproBinaryAttributeInstance";
import {RecproBinaryAttribute} from "../../../../../model/RecPro/attributes/modeling/RecproBinaryAttribute";
import {BaseAttributeComponent} from "../base-attribute/base-attribute.component";

@Component({
  selector: 'app-binary-attribute',
  templateUrl: './binary-attribute.component.html',
  styleUrls: ['./binary-attribute.component.css']
})
export class BinaryAttributeComponent extends BaseAttributeComponent{

  @Input() instance: RecproBinaryAttributeInstance = new RecproBinaryAttributeInstance();
  @Input() attribute: RecproBinaryAttribute = new RecproBinaryAttribute();

}
