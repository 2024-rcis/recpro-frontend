import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TextualAttributeComponent } from './textual-attribute.component';

describe('TextualAttributeComponent', () => {
  let component: TextualAttributeComponent;
  let fixture: ComponentFixture<TextualAttributeComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TextualAttributeComponent]
    });
    fixture = TestBed.createComponent(TextualAttributeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
