import {Component, Input} from '@angular/core';
import {
  RecproNumericAttributeInstance
} from "../../../../../model/RecPro/attributes/execution/RecproNumericAttributeInstance";
import {RecproNumericAttribute} from "../../../../../model/RecPro/attributes/modeling/RecproNumericAttribute";
import {BaseAttributeComponent} from "../base-attribute/base-attribute.component";

@Component({
  selector: 'app-numeric-attribute',
  templateUrl: './numeric-attribute.component.html',
  styleUrls: ['./numeric-attribute.component.css']
})
export class NumericAttributeComponent extends BaseAttributeComponent{
  @Input() instance: RecproNumericAttributeInstance = new RecproNumericAttributeInstance();
  @Input() attribute: RecproNumericAttribute = new RecproNumericAttribute();

}
