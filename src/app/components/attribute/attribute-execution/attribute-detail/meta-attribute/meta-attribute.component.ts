import {Component, Input} from '@angular/core';
import {RecproMetaAttribute} from "../../../../../model/RecPro/attributes/modeling/RecproMetaAttribute";
import {
  RecproMetaAttributeInstance
} from "../../../../../model/RecPro/attributes/execution/RecproMetaAttributeInstance";

@Component({
  selector: 'app-meta-attribute',
  templateUrl: './meta-attribute.component.html',
  styleUrls: ['./meta-attribute.component.css']
})
export class MetaAttributeComponent {

  @Input() attribute: RecproMetaAttribute = new RecproMetaAttribute();
  @Input() instance: RecproMetaAttributeInstance = new RecproMetaAttributeInstance();
}
