import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddAttributeComponent } from './add-attribute/add-attribute.component';
import {ClarityModule, ClrModalModule} from "@clr/angular";
import {MaterialModule} from "../../../material/material.module";
import { BinaryAttributeModelingComponent } from './base-attribute-modeling/binary-attribute-modeling/binary-attribute-modeling.component';
import { TextualAttributeModelingComponent } from './base-attribute-modeling/textual-attribute-modeling/textual-attribute-modeling.component';
import { MetaAttributeModelingComponent } from './base-attribute-modeling/meta-attribute-modeling/meta-attribute-modeling.component';
import { NumericalAttributeModelingComponent } from './base-attribute-modeling/numerical-attribute-modeling/numerical-attribute-modeling.component';
import {FormsModule} from "@angular/forms";
import { BaseAttributeModelingComponent } from './base-attribute-modeling/base-attribute-modeling.component';
import {AttributeModelingComponent} from "./attribute-modeling.component";
import { AttributeCreatorDialogComponent } from './attribute-creator-dialog/attribute-creator-dialog.component';
import { AttributeCreatorBaseComponent } from './attribute-creator-base/attribute-creator-base.component';



@NgModule({
  declarations: [
    AddAttributeComponent,
    BinaryAttributeModelingComponent,
    TextualAttributeModelingComponent,
    MetaAttributeModelingComponent,
    NumericalAttributeModelingComponent,
    BaseAttributeModelingComponent,
    AttributeModelingComponent,
    AttributeCreatorDialogComponent,
    AttributeCreatorBaseComponent
  ],
  exports: [
    AddAttributeComponent,
    BaseAttributeModelingComponent,
    AttributeModelingComponent
  ],
  imports: [
    CommonModule,
    ClrModalModule,
    MaterialModule,
    ClarityModule,
    FormsModule
  ]
})
export class AttributeModelingModule { }
