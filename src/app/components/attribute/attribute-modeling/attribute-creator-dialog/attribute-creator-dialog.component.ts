import {Component, Inject} from '@angular/core';
import {RecproTextualAttribute} from "../../../../model/RecPro/attributes/modeling/RecproTextualAttribute";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {AttributeModelingService} from "../service/attribute-modeling.service";
import {RecproAttribute} from "../../../../model/RecPro/attributes/modeling/RecproAttribute";
import {RecproAttributeType} from "../../../../model/RecPro/attributes/modeling/RecproAttributeType";
import {LoaderService} from "../../../../util/service/loader.service";

@Component({
  selector: 'app-attribute-creator-dialog',
  templateUrl: './attribute-creator-dialog.component.html',
  styleUrls: ['./attribute-creator-dialog.component.css']
})
export class AttributeCreatorDialogComponent {

  attribute: RecproAttribute = new RecproTextualAttribute();
  typeOptions = Object.values(RecproAttributeType);

  constructor(
    private dialogRef: MatDialogRef<AttributeCreatorDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public attributeData: RecproAttribute,
    private loaderService: LoaderService,
    private attributeModelingService: AttributeModelingService) {
    if (this.attributeData) {
      this.attribute = this.attributeData;
    }
  }

  cancel() {
    this.attributeModelingService.getAll();
    this.dialogRef.close();
  }

  save() {
    this.attributeModelingService.create(this.attribute);
    this.loaderService.get().subscribe(loading => {
      if (!loading) {
        this.dialogRef.close()
      }
    });
  }
}
