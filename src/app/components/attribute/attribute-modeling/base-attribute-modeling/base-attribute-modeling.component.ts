import {Component, Input} from '@angular/core';
import {RecproAttributeType} from "../../../../model/RecPro/attributes/modeling/RecproAttributeType";
import {RecproAttribute} from "../../../../model/RecPro/attributes/modeling/RecproAttribute";

@Component({
  selector: 'app-base-attribute-modeling',
  templateUrl: './base-attribute-modeling.component.html',
  styleUrls: ['./base-attribute-modeling.component.css']
})
export class BaseAttributeModelingComponent {

  typeOptions = Object.values(RecproAttributeType);

  @Input() attribute: RecproAttribute = new RecproAttribute();

}
