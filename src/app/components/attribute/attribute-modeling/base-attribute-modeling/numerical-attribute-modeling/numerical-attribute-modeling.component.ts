import {Component, Input} from '@angular/core';
import {BaseAttributeModelingComponent} from "../base-attribute-modeling.component";
import {RecproNumericAttribute} from "../../../../../model/RecPro/attributes/modeling/RecproNumericAttribute";

@Component({
  selector: 'app-numerical-attribute-modeling',
  templateUrl: './numerical-attribute-modeling.component.html',
  styleUrls: ['./numerical-attribute-modeling.component.css']
})
export class NumericalAttributeModelingComponent extends BaseAttributeModelingComponent{

  @Input() override attribute: RecproNumericAttribute = new RecproNumericAttribute();

}
