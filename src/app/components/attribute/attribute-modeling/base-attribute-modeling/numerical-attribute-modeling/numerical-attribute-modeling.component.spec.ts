import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NumericalAttributeModelingComponent } from './numerical-attribute-modeling.component';

describe('NumericalAttributeModelingComponent', () => {
  let component: NumericalAttributeModelingComponent;
  let fixture: ComponentFixture<NumericalAttributeModelingComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [NumericalAttributeModelingComponent]
    });
    fixture = TestBed.createComponent(NumericalAttributeModelingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
