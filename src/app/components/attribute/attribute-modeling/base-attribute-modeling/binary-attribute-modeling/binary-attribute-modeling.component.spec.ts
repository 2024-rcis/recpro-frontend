import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BinaryAttributeModelingComponent } from './binary-attribute-modeling.component';

describe('BinaryAttributeModelingComponent', () => {
  let component: BinaryAttributeModelingComponent;
  let fixture: ComponentFixture<BinaryAttributeModelingComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BinaryAttributeModelingComponent]
    });
    fixture = TestBed.createComponent(BinaryAttributeModelingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
