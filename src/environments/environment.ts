export const environment = {
  production: false,
  apiUrl: '/api',
  keycloak: {
    // Keycloak url
    issuer: 'http://localhost:8888',
    // Realm
    realm: 'recpro',
    clientId: 'frontend'
  },
};
